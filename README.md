# scream-pong

[![pipeline status](https://gitlab.com/mcbloch/screampong/badges/master/pipeline.svg)](https://gitlab.com/mcbloch/screampong/commits/master)
[![coverage report](https://gitlab.com/mcbloch/screampong/badges/master/coverage.svg)](https://gitlab.com/mcbloch/screampong/commits/master)

[Changelog](CHANGELOG.md)

[Contribution guide](CONTRIBUTING.md)

[License](LICENSE)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


https://www.phusionpassenger.com/library/walkthroughs/deploy/nodejs/digital_ocean/apache/oss/el7/deploy_app.html

https://www.codementor.io/codementorteam/socketio-multi-user-app-matchmaking-game-server-2-uexmnux4p

https://medium.com/@jaouad_45834/basic-chat-web-app-using-express-js-vue-js-socket-io-429588e841f0

https://medium.com/@michaelmangial1/getting-started-with-vue-js-socket-io-8d385ffb9782