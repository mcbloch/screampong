#!/bin/bash

USERNAME="screampong"

echo " ### Pulling new code ###"
cd code
su $USERNAME -c "git pull"

echo " ### Installing npm deps ###"
su $USERNAME -c "npm install"

echo " ### Building Vue files ###"
su $USERNAME -c "npm run build" 

echo " ### Restarting pm2 manager ###"
pm2 restart 0
