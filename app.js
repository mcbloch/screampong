let path = require('path');
let express = require('express');
let app = require('express')();
let server = require('http').createServer(app);
let io = require('socket.io')(server);


let { GameState } = require('./serverGame');


const ONLINE = true;

let rooms = [
    {
        id: 0,
        name: "room one",
        players: []
    },
    {
        id: 1,
        name: "room two",
        players: []
    }
];

if (ONLINE) {
    //io.origins('http://127.0.0.1:8080/pong');

    let gameState = new GameState();

    io
        .of('/game')
        .on('connection', socket => {
            gameState.joinPlayer(socket);
        });

    let lobbyNsp = io.of('/lobby');
    lobbyNsp.on('connection', socket => {
        let clientId = socket.id;
        let clientRoom;

        socket.on('joinRoom', roomId => {
            let playerList = rooms[roomId].players;
            playerList.push(clientId);
            clientRoom = roomId;
            syncRooms();

            if (playerList.length === 2) {
                startRoom(roomId);
            }
        });
        socket.on('leaveRoom', roomId => {
            let index = rooms[roomId].players.indexOf(clientId);
            rooms[roomId].players.splice(index, 1);
            clientRoom = undefined;
            syncRooms();
        });

        socket.on('disconnect', () => {
            if (clientRoom !== undefined) {
                let index = rooms[clientRoom].players.indexOf(clientId);
                rooms[clientRoom].players.splice(index, 1);
                syncRooms();
            }
        });

        syncRooms();
    });

    function syncRooms() {
        lobbyNsp.emit('syncRooms', rooms);
    }
    function startRoom(roomId) {
        let playerList = rooms[roomId].players;
        playerList.forEach(playerId => {
            lobbyNsp.to(playerId).emit('gotoGame');
        });
        playerList = [];
    }
}


const serveFolder = "dist";

app.use(express.static(serveFolder));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, serveFolder, 'index.html'));
});
app.get('/game', (req, res) => {
    res.sendFile(path.join(__dirname, serveFolder, 'game.html'));
});
app.get('/test', (req, res) => {
    res.sendFile(path.join(__dirname, serveFolder, 'test.html'));
});

server.listen(8080);
console.log(" ##### Server started ##### ");