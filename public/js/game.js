var url_string = window.location.href
var url = new URL(url_string);
var local = url.searchParams.get("local");
const ONLINE = (local !== "true")
console.log("Online: " + ONLINE);


//Aliases
let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Graphics = PIXI.Graphics,
    Text = PIXI.Text,
    TextStyle = PIXI.TextStyle;

//Create a Pixi Application
let app = new Application({
    //width: 256,         // default: 800
    //height: 256,        // default: 600
    antialias: true,    // default: false
    transparent: false, // default: false
    resolution: 1       // default: 1
}
);
//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);

app.renderer.backgroundColor = 0x061639;

// Fit canvas to screen
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.autoResize = true;
app.renderer.resize(window.innerWidth, window.innerHeight);

b = new Bump(PIXI);

// load image
loader
    .add('ball', "assets/ball.png")
    .load(setup);

//Define any variables that are used in more than one function
let ball, ballSquare, ballCircle, playerLeft, playerRight, scoreLeft, scoreRight, scoreText, waitText, state, playContainer, fpsField, errorField, debugField;
let paddleHits;
let networking;

function setup() {
    errorField = new ErrorField(app.stage);

    playContainer = new PlayContainer(app.stage, app.screen.width, app.screen.height);

    // Create and add ball
    ball = new Ball(playContainer);

    // Create and add player one and two
    playerLeft = new Player(playContainer, 40, 16, 17);
    playerRight = new Player(playContainer, playContainer.fixWidth - 60, 38, 40);

    // Init scores
    scoreLeft = 0;
    scoreRight = 0;

    paddleHits = 0;

    let style = new TextStyle({
        fontFamily: "Arial",
        fontSize: 36,
        fill: "white",
        stroke: '#ff3300',
        strokeThickness: 4,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
    });

    let message = new Text("wELcOMe tO sCrEAm pOnG!", style);
    app.stage.addChild(message);
    message.anchor.set(.5, .5);
    message.position.set(app.screen.width / 2, 50);

    let pauseStyle = new TextStyle({
        fontFamily: "Arial",
        fontSize: 36,
        fill: "white",
        stroke: '#CCC',
        alpha: 0.4,
        strokeThickness: 3,
        dropShadow: true,
        dropShadowColor: "#000000",
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
    });
    pauseText = new Text("", pauseStyle);
    app.stage.addChild(pauseText);
    pauseText.anchor.set(.5, .5);
    pauseText.position.set(app.screen.width / 2, app.screen.height / 2 - 150);

    scoreText = new TextField(app.stage, "score: {0} - {1}".format(scoreLeft, scoreRight), "white", "left", 20, 20)
    fpsField = new FpsField();
    debugField = new DebugField();

    // Set the game state
    state = waitForStart;

    if(ONLINE) 
    {
        pauseText.text = "Press r to ready up";
        networking = new Networking();
        keyboard(82).release = () => {
            networking.startGame();
            pauseText.text = "Waiting for other player";
    }
    } else {
        pauseText.text = "Press space to start";
    }

    // listen to space to unpause game
    keyboard(32).release = () => {
        state = play;
    };
    // p-key
    keyboard(80).release = () => {
        state = pause;
    }

    // Start the game loop
    app.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta) {
    // Update the current game state
    state(delta);
}

function play(delta) {
    pauseText.visible = false;

    // Move the players
    playerLeft.move();
    playerRight.move();

    // Apply velocity to ball
    ball.update();


    [playerLeft, playerRight].forEach(player => {
        let collision = b.hit(ball, player, true);
        if (collision && collision !== "bottom" && collision !== "top") {
            let magicBalancer = 50;
            let ballOffset = player.y + player.height / 2 - ball.y
            if (collision === "left") ballOffset *= -1;
            //paddleHits+=1;
            //let basespeed = paddleHits/100+0.99

            ball.vy = -(ball.vx / magicBalancer) * ballOffset;
            ball.vx *= -1;
        }
    });


    let ballHitWall = b.contain(ball, playContainer.getHitBox(), true);
    //If there's a collision, display the boundary that the collision happened on
    if (ballHitWall) {
        if (ballHitWall.has("left")) {
            scoreRight += 1;
            state = wait;
        };
        if (ballHitWall.has("right")) {
            scoreLeft += 1;
            state = wait;
        };
    }



    // Update score if relevant
    scoreText.text = "score: {0} - {1}".format(scoreLeft, scoreRight)

    // Update fps
    fpsField.update();

    // Decide who has won or not

    //Change game state to end if it is finished
}

function waitForStart(){

}

function pause(delta) {
    pauseText.visible = true;
    pauseText.text = "Paused - press space to continue"
    fpsField.reset();
}

function wait(delta) {
    ball.reset();
    playerLeft.reset();
    playerRight.reset();

    pauseText.visible = true;
    pauseText.text = "Score!! Press space to launch ball";
    fpsField.reset();
}

function error(delta) {
}

String.prototype.format = function () {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
}

class Player extends Graphics {
    constructor(stage, startX, keyUp, keyDown) {
        super();
        this.padSpeed = 5;

        let width = 20;
        let height = 100;
        // Create and add player one
        this.beginFill(0xFFFFFF);
        //player.lineStyle(2, 0x000000, 1); width-color-alpha
        this.drawRect(0, 0, width, height);
        this.endFill();
        this.x = startX;
        this.startY = stage.fixHeight / 2 - height / 2;
        this.y = this.startY;
        //this.pivot.set(width / 2, height / 2);
        this.vx = 0;
        this.vy = 0;

        // Capture keyboard arrow keys
        let up = keyboard(keyUp),
            down = keyboard(keyDown);

        //Up
        up.press = () => {
            this.vy = -this.padSpeed;
        };
        up.release = () => {
            if (!down.isDown) {
                this.vy = 0;
            }
        };

        //Down
        down.press = () => {
            this.vy = this.padSpeed;
        };
        down.release = () => {
            if (!up.isDown) {
                this.vy = 0;
            }
        };


        stage.addChild(this);

        this.wait = false;

    }

    move() {
        let newPos = this.y + this.vy;
        let newPosBottom = newPos + this.height;
        if (newPos < 0) {
            this.y = 0;
        } else if (newPosBottom > this.parent.fixHeight) {
            this.y = this.parent.fixHeight - this.height;
        } else {
            this.y = newPos;
        }
        if (!this.wait && this.vy !== 0) {
            if(ONLINE) networking.sendMove();
            this.wait = true;
        } else {
            this.wait = false;
        }
    }

    reset() {
        this.y = this.startY;
    }
}

class Ball extends Sprite {
    constructor(stage) {
        super(resources.ball.texture);
        this.startX = stage.fixWidth / 2;
        this.startY = stage.fixHeight / 2;

        this.ballSpeed = 5;

        this.vx = this.ballSpeed;
        this.vy = 0;
        this.x = this.startX;
        this.y = this.startY;
        this.anchor.set(.5, .5);

        stage.addChild(this);


        /* Debug graphics
        ballCircle = new Graphics();
        ballCircle.lineStyle(2, 0xFF00FF);
        ballCircle.drawCircle(this.width / 2, this.height / 2, this.width / 2);
        ballCircle.endFill();
        this.addChild(ballCircle);

        ballSquare = new Graphics();
        ballSquare.lineStyle(2, 0xFFBBFF);
        ballSquare.drawRect(0, 0, this.width, this.height);
        ballSquare.endFill();
        this.addChild(ballSquare); */
    }

    update() {
        this.x += this.vx;
        this.y += this.vy;
    }

    reset() {
        this.x = this.startX;
        this.y = this.startY;
        this.vx = this.ballSpeed;
        this.vy = 0;
    }
}

class PlayContainer extends PIXI.Container {
    constructor(stage, rootWidth, rootHeight) {
        super();
        let horizontalMargin = 30;

        this.fixWidth = 900
        this.fixHeight = 500;
        this.x = (rootWidth - this.fixWidth) / 2;
        this.y = (rootHeight - this.fixHeight) / 2;
        // correction to not overlap the title
        let errorTemplate = "Window size to small (need {0}px more in {1})\nplease resize you browser and refresh";
        if (this.y < 100) {
            errorField.show(errorTemplate.format(100 - this.y, "height"));
            this.visible = false;
        } else if (this.x < 0) {
            errorField.show(errorTemplate.format(0 - this.x, "width"));
            this.visible = false;
        }

        stage.addChild(this);

        let g = new Graphics();
        g.lineStyle(5, 0xCCCCCC);
        g.drawRect(0, 0, this.fixWidth, this.fixHeight);
        g.endFill();
        g.cacheAsBitmap = true;

        this.addChild(g)
    }

    // Needed for the broken collision function, this only works if the compared object are inside this container
    getHitBox() {
        return {
            x: 0,
            y: 0,
            width: this.fixWidth,
            height: this.fixHeight
        };
    }
}

class TextField extends Text {
    constructor(stage, text, color, alignment, offsetX, offsetY) {

        let style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 16,
            fill: color
        });
        super(text, style);
        this.x = 0
        this.y = offsetY;
        switch (alignment) {
            case "left":
                this.x = offsetX
                break;
            case "right":
                this.x = app.screen.width - this.width - offsetX;
                break;
        }
        stage.addChild(this);
    }
}

class FpsField extends TextField {
    constructor() {
        super(app.stage, "FPS: {0}".format(60), "yellow", "right", 10, 10);
        this.fps = 60;
        this.i = 0;
    }

    update() {
        this.i += 1;
        if (this.i === 40) {
            this.fps = 60 + (Math.floor(Math.random() * 2)) - 1;
            this._updateText();
            this.i = 0;
        }
    }

    reset() {
        this.fps = 60;
        this._updateText();
    }

    _updateText() {
        this.text = "FPS: {0}".format(this.fps);
    }
}

class DebugField extends TextField {
    constructor() {
        super(app.stage, "some placeholder".format(60), "red", "right", 10, 30);
    }

    update(text){
        this.text = text;
    }
}

class ErrorField extends Text {
    constructor(stage) {
        let style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 36,
            fill: "black",
            stroke: '#ff3300',
            strokeThickness: 4,
            dropShadow: true,
            dropShadowColor: "#000000",
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
        });
        super("", style);
        this.visible = false;
        this.x = app.screen.width / 2;
        this.y = app.screen.height / 2;
        this.anchor.set(.5, .5);
        stage.addChild(this);
    }

    show(text) {
        this.text = text;
        state = error;
        this.visible = true;
        console.log("!! Error: " + text);
    }
}


class Networking {
    constructor() {
        this.socket = io("/game", {
            'path': '/pong/socket.io'
        });
        let self = this;

        this.socket.on('id', function ({ id: id }) {
            self.id = id;
            debugField.update("id: " + id);
            console.log("Networking: id set to {0}".format(id));

            switch (id) {
                case 1:
                    self.me = playerLeft;
                    self.enemy = playerRight;
                    break;
                case 2:
                    self.me = playerRight;
                    self.enemy = playerLeft;
                    break;
            }

            self.sendMove();
            setInterval(self.sendBallUpdate.bind(self), 100);
        });

        this.socket.on('updateGameState', function ({ ball: {x: ballx, y: bally, vx: ballvx, vy: ballvy}, "1": one, "2": two}) {
            console.log("got update");
            
            state = play;
            if(self.id===1){
                self.enemy.y = two
            } else {
                self.enemy.y = one;
                ball.x = ballx;
                ball.y = bally;
                ball.vx = ballvx;
                ball.vy = ballvy;
            }
        });
    }

    sendMove() {
        //console.log("Networking: send update for player: {0}".format(this.id));
        let self = this;
        this.socket.emit('updatePosition', {
            id: self.id,
            position: {
                y: self.me.y
            }
        })
    }

    sendBallUpdate(){
        let self = this;
        this.socket.emit('updateBall', {
            ball: {
                x: ball.x,
                y: ball.y,
                vx: ball.vx,
                vy: ball.vy
            }
        })
    }

    startGame(){
        let self = this;
        this.socket.emit('start', {
        })
    }
}
