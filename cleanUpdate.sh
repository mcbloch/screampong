#!/bin/bash

USERNAME="screampong"

echo " ### Removing old code ###"
rm -rf code

echo " ### Cloning new code ###"
su $USERNAME -c "git clone https://gitlab.com/mcbloch/screampong.git code"

echo " ### Installing npm deps ###"
cd code
su $USERNAME -c "npm install"

echo " ### Building Vue files ###"
su $USERNAME -c "npm run build" 

echo " ### Restarting pm2 manager ###"
pm2 restart 0
