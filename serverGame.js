class GameState {
    constructor() {
        this.id = 0;
        this.playerOne = undefined;
        this.playerTwo = undefined;
    }

    joinPlayer(socket) {
        this.id += 1;
        if (this.id > 2) {
            console.log("Reset");
            this.playerOne = undefined;
            this.playerTwo = undefined;

            this.id = 1
        }
        if (this.id === 1) {
            this.playerOne = new Player(socket, this.id);
        } else {
            this.playerTwo = new Player(socket, this.id);
        }
        console.log(` -- set client to id: ${this.id}`);

        if (this.playerOne && this.playerTwo) {
            console.log("TWO CONNECTIONS, start updates");
            setInterval(this.sendUpdates.bind(this), 100);
        }
    }

    sendUpdates() {
        if (this.playerOne.isReady() && this.playerTwo.isReady()){
            let newGameState = {
                ball: this.playerOne.ball,
                "1": this.playerOne.y,
                "2": this.playerTwo.y
            };
            [
                this.playerOne,
                this.playerTwo
            ].forEach(player => {
                player.updateGameState(newGameState);
            });
        }
    }
}

class Player {
    constructor(socket, id) {
        this.socket = socket;
        this.id = id;
        this.y = undefined;
        this.ball = undefined;
        this.ready = false;

        this.initPlayer();
        this.listenToPlayer();
    }

    initPlayer(){
        this.socket.emit('id', {id: this.id});
    }

    listenToPlayer(){
        let self = this;
        this.socket.on('updatePosition', data => {
            self.y = data.position.y;
            //console.log("{0} | Pos update: {1}".format(self.id, self.y));
        })

        this.socket.on('updateBall', data => {
            self.ball = data.ball;
        })

        this.socket.on('start', data => {
            self.ready = true;
        })
    }

    updateGameState(newGameState) {
        this.socket.volatile.emit('updateGameState', newGameState);
    }

    isReady(){
        return this.y != undefined && this.ready;
    }
}

module.exports = {GameState}

String.prototype.format = function () {
    a = this;
    for (k in arguments) {
        a = a.replace(`{${k}}`, arguments[k])
    }
    return a
}